﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumPadButton : MonoBehaviour
{
    public Text text;
    public string value;
    public AudioManager manager;

    public void AddToText()
    {
        manager.Play("Boton");
        text.text += value;
    }
}

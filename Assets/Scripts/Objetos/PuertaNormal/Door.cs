﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool isLocked;
    public bool isNumPad;
    public GameObject[] objectsToUnlock;

    private void Start()
    {
        isLocked = true;
    }
    private void LateUpdate()
    {
        if(isLocked == false && isNumPad == false)
        {
            for(int i = 0; i < objectsToUnlock.Length;i++)
            {
                objectsToUnlock[i].SetActive(false);
            }
        }


        if(isLocked == false && isNumPad == true)
        {
            for (int i = 0; i < objectsToUnlock.Length; i++)
            {
                objectsToUnlock[i].SetActive(false);
            }
        }
        
    }
}

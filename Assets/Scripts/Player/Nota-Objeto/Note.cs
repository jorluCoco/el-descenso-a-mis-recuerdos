﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Nueva Nota", menuName = "Nota")]
public class Note : ScriptableObject
{

   
    public string descripcion;
    
    public Sprite image;


}

﻿
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName ="Nuevo Objeto", menuName = "Objeto")]
public class Objeto : ScriptableObject
{
    public string objName;
    public string descripcion;
    public float sanidad;
    public Sprite image;
    public Note _note;
    
    

}

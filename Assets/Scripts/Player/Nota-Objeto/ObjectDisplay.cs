﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDisplay : MonoBehaviour
{
    public Objeto _object;

    public string objName;
    public string objDescripcion;
    public string nDes;
    public float sanidad;
    public Sprite image;
    public Sprite noteImage;
    
     
    void Start()
    {
        this.objName = _object.name;
        this.objDescripcion = _object.descripcion;

        this.sanidad = _object.sanidad;

        this.nDes = _object._note.descripcion;
        this.image = _object.image;

        this.noteImage = _object._note.image;
         
    }

}

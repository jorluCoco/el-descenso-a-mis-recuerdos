﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sanidad : MonoBehaviour
{
    [Range(0,1)]
    public float sanidad;
    [Range(0,100)]
    public float ratePercentage;
    public float rate;  
    PlayerMovement player;
    MouseCamLook cam;
    float playerSpeed;
    float jumpHeight;
    float sensibilidad;
    public Slider slider;


    HeadBob headBob;


    float bobSpeed;
    float bobAmount;

    private void Start()
    {

        rate /= ratePercentage;
        player = GetComponent<PlayerMovement>();
        cam = GetComponentInChildren<MouseCamLook>();
        headBob = GetComponent<HeadBob>();

        playerSpeed = player.speed;
        
        sensibilidad = cam.sensibilidad;

        bobSpeed = headBob.bobSpeed;
        bobAmount = headBob.bobAmount;


       
    }
    private void Update()
    {
        SanidadEffect();

        slider.value = sanidad;

    }


    void SanidadEffect()
    {
        if (sanidad >= 1)
            sanidad = 1;
        else if (sanidad <= 0)
            sanidad = 0;

        if ((sanidad / 1) * 100 < 25)
        {
            player.speed = playerSpeed * 3;

            headBob.bobSpeed = bobSpeed * 2;
            headBob.bobAmount = bobAmount *2 ;

            cam.sensibilidad = sensibilidad * 3;
        }

        else if ((sanidad / 1) * 100 >= 25 && (sanidad / 1) * 100 < 50)
        {
            player.speed = playerSpeed * 2;
            headBob.bobSpeed = bobSpeed * 1.5f;
            headBob.bobAmount = bobAmount * 1.5f;
            cam.sensibilidad = sensibilidad * 2;
        }

        else if ((sanidad / 1) * 100 >= 50 && (sanidad / 1) * 100 < 75)
        {
            player.speed = playerSpeed * 1.5f;
            headBob.bobSpeed = bobSpeed;
            headBob.bobAmount = bobAmount;

            cam.sensibilidad = sensibilidad * 1.5f;
        }

        else
        {
            player.speed = playerSpeed;
            headBob.bobSpeed = bobSpeed;
            headBob.bobAmount = bobAmount;
            cam.sensibilidad = sensibilidad;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.gameObject.tag == "Light")
        {
            sanidad += rate * Time.deltaTime;
        }

        if(other.gameObject.tag == "Darks")
        {
            sanidad -= rate * Time.deltaTime;
        }
    }
}

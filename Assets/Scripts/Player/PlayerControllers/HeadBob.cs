﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadBob : MonoBehaviour
{
    public Vector3 restPosition; //Posicion de la camara cuando no esta moviendo 
    public float transitionSpeed = 20f; //Transicion smooth de cuando mueve a no mover
    public float bobSpeed = 5f; // Que tan rapido se mueve
    public float bobAmount = 0.05f; // Que tan dramatico es el movimiento

    public Transform camera;

    MouseCamLook mouseCamLook;
    // Start is called before the first frame update

    float timer = Mathf.PI / 2; // Sin = 1, Siempre empieza en la cretsa del seno 
    
    void Start()
    {
        mouseCamLook = GetComponentInChildren<MouseCamLook>();
    }

    // Update is called once per frame
    void Update()
    {
        if(mouseCamLook.canMove)
        {
            if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
            {
                timer += bobSpeed * Time.deltaTime;

                Vector3 newPosition = new Vector3(Mathf.Cos(timer) * bobAmount, restPosition.y + Mathf.Abs((Mathf.Sin(timer) * bobAmount)), restPosition.z);

                camera.localPosition = newPosition;
            }
            else
            {
                timer = Mathf.PI / 2;
                Vector3 newPosition = new Vector3(Mathf.Lerp(camera.localPosition.x, restPosition.x, transitionSpeed * Time.deltaTime), Mathf.Lerp(camera.localPosition.y, restPosition.y, transitionSpeed * Time.deltaTime), Mathf.Lerp(camera.localPosition.z, restPosition.z, transitionSpeed * Time.deltaTime));

                camera.localPosition = newPosition;
            }

            if (timer > Mathf.PI * 2)
                timer = 0;
        }
    }
}


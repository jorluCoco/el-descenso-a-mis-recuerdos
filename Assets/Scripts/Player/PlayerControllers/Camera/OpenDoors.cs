﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenDoors : MonoBehaviour
{
    RaycastHit hit;
    public LayerMask hitMask;
    public Text text;
    public GameObject numPadUI;
    bool enable;
    MouseCamLook camLook;
    PlayerMovement playerMovement;

    public NumPad _numPad;

    public bool correct;

    private void Start()
    {
        correct = false;
        camLook = GetComponent<MouseCamLook>();
        playerMovement = GetComponentInParent<PlayerMovement>();
    }


    private void Update()
    {
        if(Physics.Raycast(transform.position, transform.forward, out hit, 5f, hitMask))
        {

            if (hit.collider.gameObject.GetComponent<Door>().isLocked && !hit.collider.gameObject.GetComponent<Door>().isNumPad)
            {
                print("DOOR");
                
                if(Input.GetKeyDown(KeyCode.F))
                {
                    hit.collider.gameObject.GetComponent<Door>().isLocked = false;
                }
            }

           else if (hit.collider.gameObject.GetComponent<Door>().isNumPad )
            {

                _numPad = hit.collider.gameObject.GetComponent<NumPad>();
                print("NUMPAD DOOR");




                if (Input.GetKeyDown(KeyCode.F))
                {
                    enable = !enable;

                   
                    LogicCursor(enable);

                }
                if (enable)
                {
                    numPadUI.SetActive(true);
                    camLook.canMove = false;
                    playerMovement.canMove = false;
                }
                else
                {

                    numPadUI.SetActive(false);
                    camLook.canMove = true;
                    playerMovement.canMove = true;
                    numPadUI.GetComponentInChildren<Text>().text = null;
                }

                if (correct)
                {
                    hit.collider.gameObject.GetComponent<Door>().isLocked = false;
                    enable = false;
                    correct = false;
                }
            }

            
           
        }
    }


    public void Enter()
    {
        if (numPadUI.GetComponentInChildren<Text>().text == _numPad.value)
            correct = true;
    }

    public void Clear()
    {
        numPadUI.GetComponentInChildren<Text>().text = null;
    }

    

    void LogicCursor(bool locked)
    {

        if (locked)
        {
            
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

        }

        else
        {

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        
    }
}

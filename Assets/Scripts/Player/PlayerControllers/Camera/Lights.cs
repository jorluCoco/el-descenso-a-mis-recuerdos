﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lights : MonoBehaviour
{
    RaycastHit hit;
    public LayerMask hitMask;

    private void Update()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, 5f, hitMask))
        {
            if(Input.GetKeyDown(KeyCode.F))
            {
                hit.collider.gameObject.GetComponent<LightButton>().isActive = !hit.collider.gameObject.GetComponent<LightButton>().isActive;
            }
        }

    }

}




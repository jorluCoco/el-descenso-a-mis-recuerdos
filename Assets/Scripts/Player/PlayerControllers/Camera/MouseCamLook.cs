﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCamLook : MonoBehaviour
{
    public float sensibilidad = 100f;

    float xRotation;
    
    public Transform personaje;

    public bool canMove;

    // Start is called before the first frame update
    void Start()
    {
        
        canMove = true;
    }

    // Update is called once per frame
    void Update()
    {
        if( canMove)
            MouseLook();
    }
    void MouseLook()
    {

        float mouseX = Input.GetAxis("Mouse X") * sensibilidad * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensibilidad * Time.deltaTime;

        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);

        personaje.Rotate(Vector3.up * mouseX);
    }

    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Hit : MonoBehaviour
{
    
    public LayerMask hitMask;
    public Text text;
    public Text text2;
    public GameObject objectHolder;
    
    public float rotSpeed = 1000;

    public bool holding;
    public bool note;
    public bool rotate;

    public GameObject noteUI;
    public Text noteText;

     MouseCamLook mouseCamLook;
    public PlayerMovement playerMovement;

    public Inventory inventory;

    private RaycastHit hit;

    public int length;

    private void Awake()
    {
        inventory = GetComponentInParent<Inventory>();
        mouseCamLook = GetComponent<MouseCamLook>();
    }
    private void Start()
    {
        
        holding = false;
        note = false;
        rotate = false;

        length = inventory.slot.Length;
    }
    // Update is called once per frame
   
    private void LateUpdate()
    {
        UpdateLable();    
    }

    private void Update()
    {
        if (Physics.Raycast(transform.position, transform.forward, out hit, 5f,hitMask))
        { 
            if(Input.GetKeyDown(KeyCode.E) && !rotate &&!note)
            {
                if (holding)
                {

                    //suelto objeto
                    hit.collider.transform.SetParent(null);
                    //hit.collider.gameObject.layer = LayerMask.NameToLayer("Default");
                    holding = false;
                    hit.collider.gameObject.GetComponent<Rigidbody>().isKinematic = false;


                }
                else
                {
                    //agarro obj
                    hit.collider.transform.SetParent(objectHolder.transform);
                    hit.collider.transform.localPosition = Vector3.zero;
                    hit.collider.gameObject.GetComponent<Rigidbody>().isKinematic = true;
                    //hit.collider.gameObject.layer = LayerMask.NameToLayer ("Player");
                    //transform.localRotation = Quaternion.Euler(Vector3.zero);
                    holding = true;
                    
                }
            }


             if (Input.GetKeyDown(KeyCode.I) && !rotate && !note)
            {
                for (int i = 0; i < length; i++)
                {
                    if (inventory.slot[i].objeto == null)
                    {
                        inventory.slot[i].objeto = hit.collider.GetComponent<ObjectDisplay>()._object;
                        inventory.slot[i].AddToSlot();

                        Destroy(hit.collider.gameObject);

                        holding = false;
                        break;
                    }
                }
            }

            if (holding)
            {

                if (Input.GetKeyDown(KeyCode.R))
                {
                    rotate = !rotate;
                }
                
            }
            else
            {
                playerMovement.canMove = true;
                mouseCamLook.canMove = true;
                
                //RotateObject(hit.collider.gameObject);
            }


            if(rotate)
            {
                playerMovement.canMove = false;
                mouseCamLook.canMove = false;
                transform.localRotation = Quaternion.Euler(Vector3.zero);

                RotateObject(hit.collider.gameObject);
            }
            else
            {
                playerMovement.canMove = true;
                mouseCamLook.canMove = true;

            }

            if (holding && Input.GetKeyDown(KeyCode.N))
            {
                note = !note;

                if (note)
                {
                    noteUI.SetActive(true);
                    noteText.text = hit.collider.gameObject.GetComponent<ObjectDisplay>().nDes;
                }
                else
                {
                    noteUI.SetActive(false);
                    noteText.text = null;
                }
            }

            

            



            /*if (holding && objectHolder.transform.childCount < 2)
            {
                temp = hit.collider.gameObject;
                
                
                text.text = "Press N for note";
                RotateObject();
            }*/
            
        }
            
    }

    void RotateObject(GameObject go)
    {
            go.transform.Rotate(new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * rotSpeed);
    }
    
    void UpdateLable()
    {
        if (holding )
        {
            text.text = "Presiona N para leer la Nota";
            text2.text = "Presiona R para ver objeto";
            
        }
        else if(!holding && hit.transform != null)
        {
            text.text = "Presiona E para agarrar objeto";
            text2.text = "Presiona I para agregar al inventario";
        }
        else
        {
            text.text = "";
            text2.text = "";
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Inventory : MonoBehaviour
{
    bool enable;

    public GameObject inventory;
    public AudioManager manager;

    Hit mHit;

    int slots;

    public static bool onUi;

    public Slot[] slot;

    int numObj;

    private void Start()
    {
        mHit = GetComponentInChildren<Hit>();
       
       for(int i = 0;i < slot.Length;i++)
        {
            slot[i].GetComponent<Slot>().SetUp();
        }
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I) && !mHit.rotate &&!mHit.note && !mHit.holding)
        {
            enable = !enable;
            LogicCursor(enable);
        }
    }

    void LogicCursor(bool locked)
    {
        if (locked)
        {
            inventory.SetActive(true);
            manager.Play("Abrir Menu");
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;

            onUi = true;
        }

        else
        {
            inventory.SetActive(false);
            manager.Play("Cerrar Menu");
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            onUi = false;
        }
    }
}

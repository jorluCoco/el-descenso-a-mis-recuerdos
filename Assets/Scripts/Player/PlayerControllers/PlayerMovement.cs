﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public AudioManager manager;

    public bool isJumping;
    public bool isSprinting;

    public CharacterController characterController;
    public float speed;
    public float gravity;
    public bool isWalking;


    float x, z;
    float sprint;
    public float climbSpeed;
    Vector3 velocity;

    public Transform groundCheck;
    public float groundDistance = 0.5f;
    public LayerMask groundMask;

    public bool canMove;

    float normalSpeed;
    bool isGrounded;

    bool onStairs;


    public float jumpHeight;

    HeadBob headBob;


    float hAmount;
    float hSpeed;
    float hTSpeed;


    
    void Start()
    {

        headBob = GetComponent<HeadBob>();

        hAmount = headBob.bobAmount;
        hSpeed = headBob.bobSpeed;
        hTSpeed = headBob.transitionSpeed;

        canMove = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        normalSpeed = speed;

        //manager.Play("Respiracion");



    }
    // Update is called once per frame
    void Update()
    {
        if (canMove)
            PlayerMove();

       

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    void PlayerMove()
    {

        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);




        if (onStairs)
        {
            velocity.y = 0;
            gravity = 0;
        }
        else
        {
            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }
            gravity = -29.4f;

        }


        x = Input.GetAxisRaw("Horizontal");
        z = Input.GetAxisRaw("Vertical");


        if (x != 0 || z != 0 )
            isWalking = true;
        else
            isWalking = false;
       

        Vector3 move = Vector3.Normalize(transform.right * x + transform.forward * z);


        if (z > 0)
        {
            if (onStairs)
            {
                move = Vector3.Normalize(transform.right * x + transform.up * z);
                speed = climbSpeed;
            }
            else
            {
                speed = normalSpeed;
            }
        }


        


        velocity.y += gravity * Time.deltaTime;

        if (Input.GetButtonDown("Jump") )
        {
            manager.Play("Salto");
            if(isGrounded)
                velocity.y = Mathf.Sqrt(jumpHeight * -2 * gravity);
        }
    

        if (Input.GetKey(KeyCode.C))
        {
            characterController.height = 1.0f;
        }
        else
        {
            characterController.height = 3f;
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {

            isSprinting = true;

            headBob.transitionSpeed =Mathf.Lerp(hTSpeed, hTSpeed+ 1,2);
            headBob.bobSpeed =Mathf.Lerp(hSpeed,hSpeed + 1,2);
            headBob.bobAmount =Mathf.Lerp(hAmount, hAmount+ 0.5f,2);
            
            //print("Sprint");
            sprint = Mathf.Lerp(normalSpeed, normalSpeed + 4,2);
            
            speed = sprint ;
        }
        else
        {

            headBob.transitionSpeed =Mathf.Lerp(headBob.transitionSpeed, hTSpeed,2);
            headBob.bobSpeed =Mathf.Lerp(headBob.bobSpeed, hSpeed,2);
            headBob.bobAmount =Mathf.Lerp(headBob.bobAmount, hAmount,2);

            isSprinting = false;
            speed = Mathf.Lerp(sprint, normalSpeed , 2); 
            sprint = 0;
        }

        if(isSprinting && isGrounded && isWalking)
        {
            manager.Play("Sprint");

        }
        else if (!isSprinting || !isGrounded)
        {
            manager.Stop("Sprint");
        }


        if (isWalking && isGrounded )
            manager.Play("Caminado");
        else if(!isWalking || !isGrounded || isSprinting)
            manager.Stop("Caminado");

        characterController.Move(move * speed * Time.deltaTime);
        characterController.Move(velocity * Time.deltaTime);
    }

     
    private void OnTriggerStay(Collider collision)
    {
        print("Stairs");
        if(collision.gameObject.tag == "Stairs")
        {
            onStairs = true;
            
        }
    }



    private void OnTriggerExit(Collider collision)
    {
        if(collision.gameObject.tag == "Stairs")
        {
            onStairs = false;
            gravity = -29.4f;

        }
    }
}

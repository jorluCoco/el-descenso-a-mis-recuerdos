﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowImageUI : MonoBehaviour
{
    public Image uiImage;
    public Text text;

    public void Show()
    {
        uiImage.sprite = this.GetComponent<Slot>().objeto.image;

        uiImage.color = Color.white;

        text.text = this.GetComponent<Slot>().objeto.descripcion;
    }
}

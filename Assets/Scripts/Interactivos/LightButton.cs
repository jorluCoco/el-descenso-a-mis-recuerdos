﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightButton : MonoBehaviour
{
    public bool isActive;

    public GameObject[] lights;

    private void Start()
    {
        isActive = true;
    }
    private void Update()
    {
        if(isActive)
        {
            for(int i = 0; i < lights.Length;i++)
            {
                lights[i].SetActive(false);
            }
        }
        else
        {
            for (int i = 0; i < lights.Length; i++)
            {
                lights[i].SetActive(true);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    public Animator transitions;
    public float transitionTime = 1.0f;

    Button button;

    private void Start()
    {
        button = GetComponentInChildren<Button>();
    }


    // Update is called once per frame

    public void LoadNextLevel()
    {
        button.gameObject.SetActive(false);
        StartCoroutine(LoadLevel((SceneManager.GetActiveScene().buildIndex + 1)));
    }

    IEnumerator LoadLevel(int index)
    {
        transitions.SetTrigger("Start");

        yield return new WaitForSeconds(transitionTime);

        SceneManager.LoadScene(index);
    }
}

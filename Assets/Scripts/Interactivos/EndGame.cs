﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    public PlayerMovement PlayerMovement;
    public MouseCamLook camLook;
    public AudioManager manager;
    public GameObject ui;

    
    
    IEnumerator EndGameR()
    {
        ui.SetActive(true);
        PlayerMovement.canMove = false;
        camLook.canMove = false;
        manager.StopAll();

        yield return new WaitForSeconds(3.0f);
        print("FINISH");
        Application.Quit();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
            StartCoroutine(EndGameR());
    }
}

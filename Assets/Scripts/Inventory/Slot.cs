﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot :MonoBehaviour
{
    public Objeto objeto;

    string nameSlot;
    string descripcion;

    


    public void SetUp()
    {
        if(this.objeto == null)
        {
            this.GetComponent<Image>().color = new Color(255,255,255,0);
            this.GetComponent<Image>().sprite = null;
        }
    }
    public void AddToSlot( )
        
    {
        
        this.nameSlot = objeto.name;
        this.descripcion = objeto.descripcion;
        this.GetComponent<Image>().sprite = objeto.image;

        this.GetComponent<Image>().color = new Color(255, 255, 255, 1);

    }

    
}
